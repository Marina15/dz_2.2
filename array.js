var studentsAndPoints = [
	'Алексей Петров', 0, 
	'Ирина Овчинникова', 60, 
	'Глеб Стукалов', 30, 
	'Антон Павлович', 30, 
	'Виктория Заровская', 30, 
	'Алексей Левенец', 70, 
	'Тимур Вамуш', 30, 
	'Евгений Прочан', 60, 
	'Александр Малов', 0
], i, imax;

studentsAndPoints.push('Николай Фролов', 0);
studentsAndPoints.push('Олег Боровой', 0);
studentsAndPoints[7] = 40;
studentsAndPoints[studentsAndPoints.length - 3] = 10;

console.log('Список студентов:');
for (i = 0, imax = studentsAndPoints.length; i < imax; i+=2) {
	console.log('Студент ' + (studentsAndPoints[i]) + ' набрал ' + (studentsAndPoints[i + 1]) + ' баллов');
}

console.log('Студент набравший максимальный балл:');
var max, studentsAndPointsMax;
for (i = 0, imax = studentsAndPoints.length; i < imax; i+=2) {
	if (max === undefined || studentsAndPoints[i + 1] > max) {
		max = studentsAndPoints[i + 1];
		studentsAndPointsMax = i + 1;
	}
}
console.log('Студент ' + (studentsAndPoints[i]) + ' имеет максимальный балл ' + max);

console.log('Студенты не набравшие баллов:');
var min, studentsAndPointsMin;
for (i = 0, imax = studentsAndPoints.length; i < imax; i+=2) {
	if (min === undefined || studentsAndPoints[i + 1] < min) {
		min = studentsAndPoints[i + 1];
		studentsAndPointsMin = i + 1;
	}
}
console.log('Студент ' + (studentsAndPoints[i]) + ' имеет минимальный балл ' + min);
